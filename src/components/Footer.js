import React from 'react';
 
const Footer = (props) => { 
    const {isDark, filter} = props;

    let activeTasks = props.tasks.filter(obj => !obj.isCompleted).length;

    let lightTheme = isDark ? '' : 'filter-element-light';
    let activeTheme = `filter-element ${lightTheme} filter-element-active`;
    let disabledTheme = `filter-element ${lightTheme}`;

    return ( 
        <footer className={isDark ? 'task-list-footer' : 'task-list-footer light-theme-footer'}>
            <div className='active-tasks'>{activeTasks} items left</div>
            <div className='filter'>
                <span className={filter === 'all' ? activeTheme : disabledTheme} onClick={() => props.filterTasks('all')}>All</span>
                <span className={filter === 'active' ? activeTheme : disabledTheme} onClick={() => props.filterTasks('active')}>Active</span>
                <span className={filter === 'completed' ? activeTheme : disabledTheme} onClick={() => props.filterTasks('completed')}>Completed</span>
            </div>
            <span className={isDark ? 'filter-element' : 'filter-element filter-element-light'} onClick={props.removeCompletedTasks}> Clear Completed</span>
        </footer> 
        );
};
 
export default Footer;