import React, { Component } from 'react';
import checkIcon from '../images/icon-check.svg';
import checkIconBlack from '../images/icon-check-black.svg';
import crossIcon from '../images/icon-cross.svg'

class Tasks extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isMouseOver: false
        };
    }

    showCrossIcon = () => {
        this.setState({
            isMouseOver: true
        });
    };
    hideCrossIcon = () => {
        this.setState({
            isMouseOver: false
        });
    }

    render() { 
        const {isCompleted, id, taskInfo} = this.props.task;
        const {isDark} = this.props;
        return ( 
        <article className={isDark ? 'task task-block active' : 'task task-block active light-theme'} onMouseOver={this.showCrossIcon} onMouseLeave={this.hideCrossIcon}>  
            <img src={isCompleted ? checkIconBlack : checkIcon} alt='state-image' className={isCompleted ? 'check-img check-img-completed' : 'check-img'} onClick={() => this.props.changeState(id)}/>
            <span className={isCompleted ? 'task-detail comleted-task-detail' : 'task-detail'}> {taskInfo} </span>
            <img src={crossIcon} alt='remove' style={this.state.isMouseOver ? { display: 'inline-block' } : { display: 'none' }} className='close close-img' onClick={() => this.props.removeTask(id)}/>
        </article> 
        );
    }
}
 
export default Tasks;