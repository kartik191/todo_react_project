import React from 'react';
import sunIcon from '../images/icon-sun.svg'
import moonIcon from '../images/icon-moon.svg'

const Header = (props) => {       
    return ( 
        <header className='heading' > 
            <h2 className='heading-text' >TO DO</h2>
            <img src={props.isDark ? sunIcon : moonIcon} alt='theme-icon' className='theme-icon' onClick={props.changeTheme}></img>
        </header>
     );
};

export default Header;