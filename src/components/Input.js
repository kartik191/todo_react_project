import React, { Component } from 'react';

class Input extends Component {
    constructor(props) {
        super(props);
        this.state = {
            input: ''
        };
    }
    inputChange = (event) => {
        this.setState({
            input: event.target.value
        });
    };
    sumbitInput = (event) => {
        event.preventDefault();
        if(this.state.input.trim() !== ''){
            this.props.createTask(this.state.input);
            this.setState({
                input: ''
            });
        }
    };

    render() { 
        const {isDark} = this.props;
        return (
            <form className={isDark ? 'add-new-task task-block' : 'add-new-task task-block light-theme'} onSubmit={this.sumbitInput}> 
                <input type='text' value={this.state.input} onChange={this.inputChange} className={isDark ? 'input-text' : 'input-text input-text-light-theme'} placeholder='Please enter an To Do'/>
            </form> 
        );
    }
}

export default Input;