import React, { Component } from 'react';
import Input from './Input';
import Tasks from './Tasks';
import Footer from './Footer';

class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tasks: [],
            id: 0,
            filter: 'all'
        };
    }

    createTask = (taskInfo) => {
        const task = {
            id: this.state.id, 
            taskInfo, 
            isCompleted: false,
        };
        this.setState({
            tasks: [task, ...this.state.tasks],
            id: this.state.id + 1, 
        });
    };
    changeState = (id) => {
        const tasks = [...this.state.tasks].map(task => {
            if(task.id === id){
                return {
                    ...task,
                    isCompleted: task.isCompleted ? false : true
                };
            }
            return task;
        });
        this.setState({
            tasks
        });
    };
    removeTask = (id) => {
        const tasks = this.state.tasks.filter(task => task.id === id ? false : true);
        this.setState({
            tasks
        });
    };
    removeCompletedTasks = () => {
        const tasks = this.state.tasks.filter(task => task.isCompleted ? false : true);
        this.setState({
            tasks
        });
    };
    filterTasks = (filter) => {
       this.setState({
            filter
        });
    };
    getFilteredTasks(){
        if(this.state.filter === 'active'){
            return this.state.tasks.filter(task => !task.isCompleted);
        }
        else if(this.state.filter === 'completed'){
            return this.state.tasks.filter(task => task.isCompleted);
        }
        else {
            return this.state.tasks;
        }
    }
    render() { 
        const {isDark} = this.props;
        return (  
           <main>
                <Input createTask={this.createTask} isDark={isDark}/>
                <section className='task-list'>
                    {
                       this.getFilteredTasks().map(task => <Tasks key={task.id} task={task} isDark={isDark} changeState={this.changeState} removeTask={this.removeTask}/>)
                    }
                </section>
                <Footer tasks={this.state.tasks} filter={this.state.filter} isDark={isDark} removeCompletedTasks={this.removeCompletedTasks} filterTasks={this.filterTasks}/>
           </main>
        );
    }
}
 
export default Main;