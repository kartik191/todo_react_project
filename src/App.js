import React, { Component } from 'react';
import './App.css';
import Main from './components/Main'
import Header from './components/Header'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isDark: true
    };
  }
  changeTheme = () => {
    this.setState({ 
      isDark: this.state.isDark ? false : true 
    });
  };
  render() { 
    const {isDark} = this.state;
    return (
      <div className={isDark ? 'body': 'body body-light' }>
        <Header isDark={isDark} changeTheme={this.changeTheme}/>
        <Main isDark={isDark}/>
      </div>
    );
  }
}

export default App;
